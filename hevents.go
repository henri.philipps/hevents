package hevents

import (
	"context"
	"time"
)

// AggregateBase defines the basic methods of an aggregate.
type AggregateBase interface {
	AggregateID() string
	AggregateType() string
}

// Aggregate is representing the current state of an entity and contains the
// business logic to handle commands.
type Aggregate interface {
	AggregateBase
	CommandHandler
	Data() any
}

// Event is an immutable record of something that happened in the system.
type Event interface {
	AggregateID() string
	AggregateType() string
	EventType() string
	Timestamp() time.Time
	Version() uint64
	Data() any
}

// Command is an action that is handled by an aggregate, changing its state.
type Command interface {
	AggregateID() string
	AggregateType() string
	CommandType() string
	Data() any
}

// EventHandler is an interface for handling events, e.g. for subscribers on a event bus.
type EventHandler interface {
	HandlerType() string
	HandleEvent(context.Context, Event) error
}

// CommandHandler is implemented by Aggregates to handle commands.
type CommandHandler interface {
	HandleCommand(context.Context, Command) error
}

// CommandHandlerFunc is a function that handles commands and implements CommandHandler.
type CommandHandlerFunc func(ctx context.Context, cmd Command) error

// HandleCommand implements CommandHandler for CommandHandlerFunc.
func (f CommandHandlerFunc) HandleCommand(ctx context.Context, cmd Command) error {
	return f(ctx, cmd)
}

// EventStore is an interface for storing events.
type EventStore interface {
	Save(ctx context.Context, events []Event, originalVersion int) error
	Load(ctx context.Context, id string) ([]Event, error)
	LoadFrom(ctx context.Context, id string, version int) ([]Event, error)
	Close() error
}

// AggregateStore is an interface for storing aggregates.
type AggregateStore interface {
	Save(ctx context.Context, aggregate Aggregate) error
	Load(ctx context.Context, aggregateType string, id string) (Aggregate, error)
}
