package aggregate

import (
	"context"
	"fmt"

	"github.com/oklog/ulid/v2"
	"gitlab.com/henri.philipps/hevents"
)

var _ hevents.AggregateBase = (*AggregateBase)(nil)

// AggregateBase is implementing the AggregateBase interface and is used as an
// utility to instantiate Aggregates.
type AggregateBase struct {
	ID       string
	Type     string
	Ver      uint64
	Events   []hevents.Event
	handlers map[string]hevents.CommandHandler
}

// NewBase creates a new AggregateBase and is mainly used by New().
func NewBase(typeName string, id string, handlers map[string]hevents.CommandHandler) *AggregateBase {
	var uid string
	if id == "" {
		uid = ulid.Make().String()
	} else {
		uid = id
	}

	var h map[string]hevents.CommandHandler
	if handlers != nil {
		h = handlers
	} else {
		h = map[string]hevents.CommandHandler{}
	}

	return &AggregateBase{
		ID:       uid,
		Type:     typeName,
		Ver:      1,
		Events:   []hevents.Event{},
		handlers: h,
	}
}

func (a *AggregateBase) Version() uint64 {
	return a.Ver
}

func (a *AggregateBase) AggregateID() string {
	return a.ID
}

func (a *AggregateBase) AggregateType() string {
	return a.Type
}

// CommandBase contains the common fields for all commands.
// It is used as an utility to instantiate Commands.
type CommandBase struct {
	AID   string
	AType string
	Type  string
}

// NewCommandBase creates a new CommandBase and is mainly used by NewCommand().
func NewCommandBase(aggregateID string, aggregateType string, commandType string) CommandBase {
	return CommandBase{
		AID:   aggregateID,
		AType: aggregateType,
		Type:  commandType,
	}
}

func (cmd CommandBase) AggregateID() string {
	return cmd.AID
}

func (cmd CommandBase) AggregateType() string {
	return cmd.AType
}

func (cmd CommandBase) CommandType() string {
	return cmd.Type
}

var _ hevents.Command = (*Command[any])(nil)

// Command is a parametric type representing a command. The type parameter T is
// defining the type of the Payload to ensure compile time type checking and
// prevent having to make type assertions in CommandHandlers.
type Command[T any] struct {
	CommandBase
	Payload T
}

// NewCommand creates a new Command with a parametric Payload type.
func NewCommand[T any](aggregateID string, aggregateType string, commandType string, payload T) Command[T] {
	return Command[T]{
		CommandBase: NewCommandBase(aggregateID, aggregateType, commandType),
		Payload:     payload,
	}
}

// Data returns the payload of the command and is needed to implement the
// Command Interface. For type-safety better use DataGeneric.
func (cmd Command[T]) Data() any {
	return cmd.Payload
}

// DataGeneric returns the payload of the command with the type
// determined by the type parameter T.
func (cmd Command[T]) DataGeneric() T {
	return cmd.Payload
}

var _ hevents.Aggregate = (*Aggregate[any])(nil)

// Aggregate is a parametric type representing an aggregate. The type parameter T
// is defining the type of the business Object to ensure compile time type checking and
// prevent having to make type assertions in CommandHandlers.
type Aggregate[T any] struct {
	*AggregateBase
	Object T
}

// New is creating a new Aggregate with a parametric Object type determined by T.
func New[T any](typeName string, id string, object T) *Aggregate[T] {

	return &Aggregate[T]{
		AggregateBase: NewBase(typeName, id, map[string]hevents.CommandHandler{}),
		Object:        object,
	}
}

// Data returns the business Object of the aggregate with the type determined
// by the type parameter T.
func (a *Aggregate[T]) Data() T {
	return a.Object
}

// AddHandler is a parametric helper function to add Handlers to an Aggregate
// and determine the type of the embedded Object in the Aggregate and the
// embedded Payload in the Command by type paramters. This makes it easy to add handler
// functions without having to make type assertions.
//
// For example:
//
//	// to be used as Object in the Aggregate
//	type User struct {
//	    Name string
//	}
//
//	// to be used as Payload in the Command
//	type SetNameCommand struct {
//	    Name string
//	}
//
//	AddHandler[*User, SetNameCommand](aggregate, "SetName", func(ctx context.Context, cmd Command[SetNameCommand]) {
//	    aggregate.Data().Name = cmd.DataGeneric().Name
//	    return nil
//	})
func AddHandler[O any, D any](a *Aggregate[O], cmdType string, f genericCommandHandlerFunc[D]) {
	a.handlers[cmdType] = f
}

// HandleCommand is implementing the CommandHandler interface.
func (a *Aggregate[T]) HandleCommand(ctx context.Context, cmd hevents.Command) error {
	if cmd.AggregateType() != a.AggregateType() {
		return fmt.Errorf("aggregate type mismatch: %s != %s", cmd.AggregateType(), a.AggregateType())
	}

	if cmd.AggregateID() != a.AggregateID() {
		return fmt.Errorf("aggregate id mismatch: %s != %s", cmd.AggregateID(), a.AggregateID())
	}

	if h, ok := a.handlers[cmd.CommandType()]; ok {
		return h.HandleCommand(ctx, cmd)
	}

	return fmt.Errorf("command type %s not supported", cmd.CommandType())
}

var _ hevents.CommandHandler = (genericCommandHandlerFunc[any])(nil)

// genericCommandHandlerFunc is a generic function to handle a Commands and used by
// AddHandler().
type genericCommandHandlerFunc[D any] func(ctx context.Context, cmd Command[D]) error

// HandleCommand is implementing the CommandHandler interface.
func (f genericCommandHandlerFunc[D]) HandleCommand(ctx context.Context, cmd hevents.Command) error {
	c, ok := cmd.(Command[D])
	if !ok {
		return fmt.Errorf("command %s could not be asserted to the expected type", cmd.CommandType())
	}
	return f(ctx, c)
}
