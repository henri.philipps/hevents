package aggregate

import (
	"context"
	"testing"
)

func TestAggregate(t *testing.T) {

	type Person struct {
		Name string
		Age  int
	}

	type Account struct {
		Person
		Balance int
	}

	pAggr := New[*Person]("PERSON", "", &Person{})
	aAggr := New[*Account]("ACCOUNT", "", &Account{})

	nameCmd := NewCommand[Person](pAggr.AggregateID(), pAggr.AggregateType(), "SetName", Person{Name: "John"})
	ageCmd := NewCommand[Person](pAggr.AggregateID(), pAggr.AggregateType(), "SetAge", Person{Age: 11})
	balanceCmd := NewCommand[int](aAggr.AggregateID(), aAggr.AggregateType(), "SetBalance", 100)
	bogusCmd1 := NewCommand[Account](pAggr.AggregateID(), pAggr.AggregateType(), "SetAge", Account{Person{Age: 11}, 100})

	AddHandler[*Person, Person](pAggr, "SetName", func(ctx context.Context, cmd Command[Person]) error {
		pAggr.Data().Name = cmd.DataGeneric().Name
		return nil
	})

	AddHandler[*Person, Person](pAggr, "SetAge", func(ctx context.Context, cmd Command[Person]) error {
		pAggr.Data().Age = cmd.DataGeneric().Age
		return nil
	})

	AddHandler[*Account, int](aAggr, "SetBalance", func(ctx context.Context, cmd Command[int]) error {
		aAggr.Data().Balance = cmd.DataGeneric()
		return nil
	})

	ctx := context.Background()

	if err := pAggr.HandleCommand(ctx, nameCmd); err != nil {
		t.Error(err)
	}

	if err := pAggr.HandleCommand(ctx, ageCmd); err != nil {
		t.Error(err)
	}

	if err := aAggr.HandleCommand(ctx, balanceCmd); err != nil {
		t.Error(err)
	}

	if err := pAggr.HandleCommand(ctx, balanceCmd); err == nil {
		t.Errorf("expected Error because of not supported command type but got nil")
	}

	if err := pAggr.HandleCommand(ctx, bogusCmd1); err == nil {
		t.Errorf("expected error because of mismatching types but got nil")
	}

	if want, got := "John", pAggr.Object.Name; want != got {
		t.Errorf("expected %s, got %s", want, got)
	}

	if want, got := 11, pAggr.Object.Age; want != got {
		t.Errorf("expected %d, got %d", want, got)
	}

	if want, got := 100, aAggr.Object.Balance; want != got {
		t.Errorf("expected %d, got %d", want, got)
	}
}
