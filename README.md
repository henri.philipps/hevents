# Event Sourcing and CQRS

## Types and Interfaces

### Entity

Unique domain object.

### Aggregate

Manages authoritative state and domain logic of an `Entity`. Handles Commands
and stores them as Events. Can apply Events and emit Events.

### Command

Represents the intention to execute an action. Should have `AggregateID` and
`Payload`.

### Event

Immutable record of something that happened in the system. Should have
`AggregateID` of affected Aggregate, `Version` and `Payload`.

### EventStore

Provides methods for storing and retrieving Events (like `Append()`,
`GetEventsByAggregateID()`, `GetEventsByType()`)

### AggregateStore

Provides methods to store an Aggregate. It can do so by using an `EventStore`.
Optionally can also store snapshots of an Aggregate.

### CommandBus

Dispatches Cammands to CommandHandlers.

### EventBus

Allows to publish and to subscribe to Events.

